'''@file                    ledGameUtime.py
   @brief                   Brief doc for ledGameUtime.py
   @details                 Detailed doc for ledGameUtime.py 
   @author                  Anthony Vuong
   @date                    April 28, 2021
'''


import random, pyb, time, utime, sys

#global variables for utimer values
isrTime = 0
ledOnTime = 0

#interrupt callback function
def count_isr (which_pin):       
    global isrTime
    isrTime = utime.ticks_us()

#blink led for 1 second on PA6 LED
def blinkLed():
    blinko = pyb.Pin(pyb.Pin.board.PA6, mode=pyb.Pin.OUT_PP)
    global ledOnTime
    ledOnTime = utime.ticks_us()
    blinko.high()
    time.sleep(1)
    blinko.low()
    

#Develop a random integer to generate a 2-3 second delay
def randomWait():
    randTime = random.randrange(2000000, 3000000)
    
    utime.sleep_us(randTime)


#Run program    
if __name__ == "__main__":
    
    #External interrupt from blue button on NUCLEO - PC13
    extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_RISING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)                   # Interrupt service routine
    
    
    #Total time of each timing session added together  
    totalTime = 0

    #Number of time sessions
    timeSessions =  0
    
    while True:
        #Try and catch for Keyboard Interrupt
        try:
            #Wait for a time between 2-3 seconds
            randomWait()    
           
            blinkLed()
            
            #Calculating time difference
            timeDiff = utime.ticks_diff(isrTime, ledOnTime)
            
            #Button Not Pressed
            if timeDiff < 0 or timeDiff > 1000000:
                print("Button was not pressed")
             #Button Pressed, log time
            else:
                print(str(timeDiff))
                
                totalTime += timeDiff
                timeSessions += 1
        except KeyboardInterrupt:
               #Calculate average and print to user
               print("Average reaction time in seconds: " + str((totalTime / timeSessions) / 1000000))
               #Exit from program gracefully
               sys.exit()
     
    

    




