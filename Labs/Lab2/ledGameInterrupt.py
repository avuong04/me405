'''@file                    ledGameInterrupt.py
   @brief                   Brief doc for ledGameInterrupt.py
   @details                 Detailed doc for ledGameInterrupt.py 
   @author                  Anthony Vuong
   @date                    April 28, 2021
'''

import pyb, time, sys
import micropython
micropython.alloc_emergency_exception_buf(100)

#global variables for timer values

buttonTime = 0
last_compare = 0

#Onboard Led
onBLedPin = pyb.Pin(pyb.Pin.board.PA5, pyb.Pin.OUT_PP)
#PB3 receives input compare
buttonPin = pyb.Pin(pyb.Pin.board.PB3)

#Timer 2 initialized to tick to 25199, each second is equivalent to timer count 9999
timer_2 = pyb.Timer(2, prescaler = 9999, period=25199, mode=pyb.Timer.UP)

#Timer 2 Channel 1 initialized to toggle on matching output compare value
channel_1 = timer_2.channel(1, mode=pyb.Timer.OC_TOGGLE, polarity=pyb.Timer.LOW, pin=onBLedPin)

#Timer 2 Channel 2 initialized to receive input capture from PB5
channel_2 = timer_2.channel(2, mode=pyb.Timer.IC, polarity=pyb.Timer.HIGH, pin=buttonPin)

#Output compare interrupt callback function
def ocCallBack(timer_2):
    
    global last_compare
    last_compare = channel_1.compare()
    #Channel compare value set to 2 seconds (2*9999 = 19998), reduces LED on time to ~1 sec
    if last_compare == 0:
        last_compare += 19998
    #Turn off led for 3 seconds again
    else:
        last_compare = 0
    channel_1.compare(last_compare)
    
#Input compare callback interrupt 
def icCallBack(timer_2):
    global buttonTime
    buttonTime = channel_2.capture()
   
#External interrupt from blue button on NUCLEO - PC13 -> PB3 through jumper wire    
extint = pyb.ExtInt (pyb.Pin.board.PC13,  # Which pin
             pyb.ExtInt.IRQ_RISING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             icCallBack)                   # Interrupt service routine

#Timer 2 Channel 1 Interrupt Callback Function
channel_1.callback(ocCallBack)

#Timer 2 Channel 2 Interrupt Callback Function
channel_2.callback(icCallBack)
  
#Total time of each timing session added together  
totalTime = 0

#Number of time sessions
timeSessions =  0

#Try and catch for Keyboard Interrupt
try:
    
    while True:
        #Calculating time difference
        timeDiff = buttonTime - last_compare
        
        #Button Pressed, convert time and log
        if(buttonTime > 19998 and last_compare == 19998):
            timeSessions += 1
            time = (timeDiff * 9999) / 80
            print(str(time))
            totalTime += time
            timeDiff = 0
        #Button Not Pressed
        elif(last_compare == 0 and timeDiff > 19998):
            print(str(timeDiff))
         
        
                
except KeyboardInterrupt:
    #Calculate average and print to user
    print("Average reaction time in seconds: " + str((totalTime / timeSessions) / 1000000))
    #Exit from program gracefully
    sys.exit()








