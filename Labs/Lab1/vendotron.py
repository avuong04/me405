'''@file                    vendotron.py
   @brief                   Brief doc for vendotron.py
   @details                 Detailed doc for vendotron.py 
   @author                  Anthony Vuong
   @date                    April 21, 2021
'''
from time import sleep
from changeUtil import getChange, printChange, insertChangeHelper, displayChangeOptions
from sodaUtil import chooseSoda


class vendotron:
    '''@brief               Vendotron object
       @author              Anthony Vuong
       @date                April 21, 2021
    '''
    
    def __init__(self):
        '''@brief           Constructor for vendotron driver
           @param           None
           @details         Detailed info on vendotron driver constructor
           @return          None
        '''
        self.state = 0
        self.price = 0
        self.payment = 0
        self.balance = 0
        
    
    def vendotronTask(self):
        ''' @brief           A function for finite state machine
            @details         To exit program, press hotkey ctrl 'C'
            @param           None
            @return          Yields the FSM next state      
        '''
        while True:
            
            if self.state == 0: 
                self.displayInitMessage()      
            
            elif self.state == 1:
                self.idleState()
            
            elif self.state == 2:
                self.positiveState()
            
            elif self.state == 3:
                self.negativeState()
            
            else:            
                pass
            
            yield(self.state)
        
        
    def idleState(self):
        '''@brief           Idle state operations of Vendotron
           @param           None
           @details         Detailed info on idleState function
           @return          None, changes Vendotron state based on different conditions
        '''
        self.price = chooseSoda(self.balance);
        sleep(2)
        
        if(self.price == 'E'):
            self.state = 2
        else:
            
            t = self.insertChange()
            
            if(t == 'E'):
                self.state = 2
            else:
                if(self.balance > 0 and self.balance >= self.price):
                    self.dispenseSoda()
                    self.state = 2
                else:
                    self.state = 3 
    
    def positiveState(self):
        '''@brief           'Positive' state operations of Vendotron
           @param           None
           @details         Customer has enough balance to dispense desired drink
           @return          returns change payment in form of list
        '''
        result = getChange(self.balance)
        if(self.balance == 0):
            print("\t\t\tSorry, no change!")
        else:
            print("\t\t\tCalculating Change...\n")
            
        printChange(result)
        self.balance = 0
        self.state = 1 
    
    def displayInitMessage(self):
        '''@brief           Initial state operations of Vendotron
           @param           None
           @details         Displays the initial state message
           @return          None
        '''
        print("\n\n\t\t\t\t\t\t\tWELCOME TO VENDOTRON")
        print("\n\t\t\t\tVendotron accepts 20/10/5/1/.25/.10/.05/.01\n")
        sleep(3)
        self.state = 1
    
    def negativeState(self):
        '''@brief           Negative state operations of Vendotron
           @param           None
           @details         Customer does not have enough balance, prompts user to insert more change
           @return          None
        '''
        print("Balance low")
        t = self.insertChange()
        if(t == 'E'):
               self.state = 2
        else:
    
            if(self.balance > 0 and self.balance > self.price):
                self.dispenseSoda()
                self.state = 2
        
        
    def insertChange(self):
        '''@brief           Function to receive change from customer
           @param           None
           @details         Used to collect balance into object variable 'balance'
           @return          the total balance from change input
        '''

        t = ''
        displayChangeOptions()
        loop = True
        
        while loop:
            t = insertChangeHelper(self.balance)
            if(t == "S" or t == 'E'):
                loop = False
            elif(t == "B"):
                print("\n\t\t\tBalance: " + str(float(self.balance)))
            else:
                self.balance = t
                t =''
              
        return t
        
    
    def dispenseSoda(self):
        '''@brief           Vendotron dispensing soda
           @param           None
           @details         Object variable 'balance' is updated, soda prompt is printed
           @return          None, changes Vendotron state based on different conditions
        '''
        self.balance -= self.price
        print("\n\t\t\tRemaining Balance: " + "{:.2f}".format(float(self.balance)))
        print("\t\t\tSoda dispensed!")
        sleep(1)
    
           
   
        
        
        
        
              
   
    
        
   
            
   