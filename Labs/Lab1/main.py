'''@file                    main.py
   @brief                   Brief doc for main.py
   @details                 Detailed doc for main.py 
   @author                  Anthony Vuong
   @date                    April 21, 2021
'''
from vendotron import vendotron
from time import sleep

if __name__ == "__main__":
    '''@brief           Driver functions 
       @param           None
       @details         Driver function for vendotron soda machine
       @return          None
    '''
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator objectc

    vendo = vendotron()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stopsC yielding.C

    try:
        while True:
            next(vendo.vendotronTask())
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')