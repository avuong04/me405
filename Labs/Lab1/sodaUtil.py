'''@file                    sodaUtil.py
   @brief                   Brief doc for sodaUtil.py
   @details                 Detailed doc for sodaUtil.py 
   @author                  Anthony Vuong
   @date                    April 21, 2021
'''
import keyboard
last_key = ''

def kb_cb(key):
    '''@brief           Callback function
       @param           Callback function which is called when a key has been pressed.
       @return          None
    '''
    global last_key
    last_key = key.name
    

def initSodayKeys():
    '''@brief           Initializes soda keys
       @details           Allows key presses for specific soda inputs
       @return          None
    '''
    # Tell the keyboard module to respond to these particular keys only
    keyboard.on_release_key("C", callback=kb_cb)
    keyboard.on_release_key("P", callback=kb_cb)
    keyboard.on_release_key("S", callback=kb_cb)
    keyboard.on_release_key("D", callback=kb_cb)
    keyboard.on_release_key("E", callback=kb_cb)
    

def display(balance):
    '''@brief           Displays soda options
       @details         Prints soda options for customer
       @return          None
    '''
    print("\n\n\t\tChoose a drink!")
    print("""\t\tC - CUKE
    \tP - POPSI
    \tS - SPRYTE
    \tD - DR. PUPPER
    \tE - EJECT CHANGE
          """)       
    print("\n\t\tBalance: " + str(balance))

def chooseSoda(balance):
    '''@brief           Soda option keys 
       @param           Balance - vendotron object variable
       @details         Handles the customerssoda input through specified key presses
       @return          price of selected soda
    '''
    initSodayKeys()
    display(balance)
    price = 0
    global last_key

    while True:
        if last_key == 'E':
            last_key = ''
            print("{0:12}EJECT CHANGE".format(" "))
            price = 'E'
            keyboard.unhook_all()
            break
        elif last_key == 'C':
            last_key = ''
            print("{0:12}CUKE costs 1.00".format(" "))
            price = float(1.00)
            keyboard.unhook_all()
            break
        elif last_key == 'P':
            last_key = ''
            print("{0:12}POPSI costs 1.20".format(" "))
            price = float(1.2)
            keyboard.unhook_all()
            break
        elif last_key == 'S':
            last_key = ''
            print("{0:12}SPRYTE costs 0.85".format(" "))
            price = float(0.85)
            keyboard.unhook_all()
            break
        elif last_key == 'D':
            last_key = ''
            print("{0:12}DR. PUPPER costs 1.10".format(" "))
            price = float(1.1)
            keyboard.unhook_all()
            break
        elif keyboard.is_pressed('Q'):
            keyboard.unhook_all()
            break
    
    return price
        
        


    
        
        
