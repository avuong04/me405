'''@file                mainpage.py
   @brief               Main page documentation
   @details             ME405 Documentations Details
   
   @mainpage

   @section sec_intro   Introduction
                        ME405 Documentation for lab/project/homework files.

   @subsection Lab1     Lab1 - Vendotron - FSM wth Python Practice; Link to Bitbucket files here: https://bitbucket.org/avuong04/me405/src/master/Labs/Lab1/ 
            
   @subsection Lab2     Lab2 
    
   @subsection Lab3     Lab3 
    
   @subsection Lab4     Lab4   
    
   @author             Anthony Vuong
   
   @date               April 14, 2021
'''

