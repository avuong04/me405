'''@file                    changeUtil.py
   @brief                   Brief doc for changeUtil.py
   @details                 Detailed doc for changeUtil.py 
   @author                  Anthony Vuong
   @date                    April 21, 2021
'''
import keyboard

last_key = ''

#Helper function calculates NON-ZERO change
def getChangeHelper(value):
    '''@brief           Calculating change helper
       @param           Value of change
       @details         Calculates change in bills and cents
       @return          returns the bills and cents in list form
    '''
    counter = 0
    result = []
    change = [20.0, 10.0, 5.0, 1.0, .25, .10, .05, 0.01]
    for num in change:
        counter = value / num
        result.insert(0, str(int(counter)))
        value = round(value % num, 2)
          
    return result

def printChange(result):
    '''@brief           Prints change
       @param           change in form of list
       @details         Prints change in specified format
       @return          None
    '''
    print("\n\t\t\tpayment = (", end='')
    for i in range(8):
        if(i == 7):
            print(result[i], end='')
        else:
            print(result[i], end=', ')
      
    print(")")
    result.clear()
    print("\n")

#Returns a list of change
def getChange(change):
    '''@brief           Gets the change for customer
       @param           float param representing change
       @details         Calculates and prints the correct change
       @return          returns the change in specified format - [dollars/cents]
    '''
    return getChangeHelper(change)


def displayChangeOptions():
    '''@brief           Prints customer change options
       @details         Prompts user to insert correct bills and coinds
       @return          None
    '''
    print("""\n\t\tInsert Change: 
          \t\t0 - 0.01
          \t\t1 - 0.05
          \t\t2 - 0.10
          \t\t3 - 0.25
          \t\t4 - 1.00
          \t\t5 - 5.00
          \t\t6 - 10.00
          \t\t7 - 20.00
          \t\tE - EJECT_CHANGE
          \t\tS - SELECT SODA
          \t\tB - BALANCE""")

def kb_cb(key):
    '''@brief           Callback function
       @details           Callback function which is called when a key has been pressed.
       @return          None
    '''
    global last_key
    
    last_key = key.name


def initChangeKeys():
    '''@brief           Initializes change keys
       @details         Allows key presses for specific change inputs
       @return          None
    '''
    # Tell the keyboard module to respond to these particular keys only   
    keyboard.on_release_key("B", callback=kb_cb)
    keyboard.on_release_key("E", callback=kb_cb)
    keyboard.on_release_key("0", callback=kb_cb)
    keyboard.on_release_key("1", callback=kb_cb)
    keyboard.on_release_key("2", callback=kb_cb)
    keyboard.on_release_key("3", callback=kb_cb)
    keyboard.on_release_key("4", callback=kb_cb)
    keyboard.on_release_key("5", callback=kb_cb)
    keyboard.on_release_key("6", callback=kb_cb)
    keyboard.on_release_key("7", callback=kb_cb)
    keyboard.on_release_key("S", callback=kb_cb)

  

def insertChangeHelper(balance):
    '''@brief           Change option keys 
       @param           Balance - vendotron object variable
       @details         Handles the customers change input through specified key presses
       @return          updated vendotron balance
    '''
    initChangeKeys()
    global last_key
   
    if last_key == 'E':
        last_key = ''
        balance = 'E'
        keyboard.unhook_all()
    elif last_key == '0':
        last_key = ''
        balance += float(.01)      
    elif last_key == '1':
        last_key = ''
        balance += float(.05)
    elif last_key == '2':
        last_key = ''
        balance += float(.10)
    elif last_key == '3':
        last_key = ''
        balance += float(.25)
    elif last_key == '4':
        last_key = ''
        balance += float(1.00)
    elif last_key == '5':
        last_key = ''
        balance += float(5.00)
    elif last_key == '6':
        last_key = ''
        balance += float(10.00)
    elif last_key == '7':
        last_key = ''
        balance += float(20.00)
    elif last_key == 'S':
        last_key = ''
        balance = 'S'
        keyboard.unhook_all()
    elif last_key == 'B':
        last_key = ''
        balance = 'B'
  
        
    return balance  
   
   
    
    

