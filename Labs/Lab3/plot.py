'''@file                    plot.py
   @brief                   Brief doc for plot.py
   @details                 Detailed doc for plot.py 
   @author                  Anthony Vuong
   @date                    May 6, 2021
'''

import array
from matplotlib import pyplot


time = array.array('f')
data = array.array('f')

with open('dataFile.csv','r') as file:
    for line in file:
        print("Line= ", line)
        print("Stripped line=", line.strip())
        print("Split Line=", line.strip().split(','))
        [t, y] = line.strip().split(',')
        print("t= ", t)
        print("y= ", y)
        time.append(float(t))
        data.append(float(y))

pyplot.figure()
pyplot.plot(time, data)
pyplot.xlabel('Time, t')
pyplot.ylim([-1, 1])
pyplot.yticks([-1, -0.5, 0, 0.5, 1])
pyplot.ylabel('Output, y')
pyplot.title('y vs t')

