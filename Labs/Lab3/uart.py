'''@file                    uart.py
   @brief                   Brief doc for uart.py
   @details                 Detailed doc for uart.py 
   @author                  Anthony Vuong
   @date                    May 6, 2021
'''

from pyb import UART

txPin = pyb.Pin('PA0', mode=pyb.Pin.AF_PP, af = 8)
rxPin = pyb.Pin('PA1', mode=pyb.Pin.AF_PP, af = 8)

uartMode = UART(4, txPin, rxPin)

while True:
    if uartMode.any() != 0:
        val = uartMode.readchar()
        uartMode.write('You sent ' + str(val) + ' to Nucelo')
        
        
        
# print(txPin.mode())
# print(rxPin.mode())
# 
# print(txPin.af_list())
# print(rxPin.af_list())




