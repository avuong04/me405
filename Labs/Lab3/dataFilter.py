'''@file                    dataFilter.py
   @brief                   Brief doc for dataFilter.py
   @details                 Detailed doc for dataFilter.py 
   @author                  Anthony Vuong
   @date                    May 6, 2021
'''
def analyzeData(data):
    lastIndex = len(data) - 1
    firstIndex = 0
    dataPointDiff = data[lastIndex] - data[firstIndex]
    tolerance = 5000 * .10   #tolerance +/- percentage, change if desired
    positiveDataTolerance = 5000 + tolerance
    negativeDataTolerance = 5000 - tolerance
    if(dataPointDiff < positiveDataTolerance and dataPointDiff > negativeDataTolerance):
        
        