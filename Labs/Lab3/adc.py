'''@file                    adc.py
   @brief                   Brief doc for adc.py
   @details                 Detailed doc for adc.py 
   @author                  Anthony Vuong
   @date                    May 6, 2021
'''

import pyb, array


def interruptADC():
    return none

if __name__ == '__main__':


    capturePin = pyb.Pin(pyb.Pin.cpu.A3, pyb.Pin.ANALOG)
    adc = pyb.ADC(capturePin)
    timer_2 = pyb.Timer(2, freq = 10)
    buf = array.array ('H', (0 for index in range (200)))

    adc.read_timed(buf, timer_2)

    for val in buf:                     # loop over all values
        print(val)   






