'''@file                    serial.py
   @brief                   Brief doc for serial.py
   @details                 Detailed doc for serial.py 
   @author                  Anthony Vuong
   @date                    May 6, 2021
'''

import serial

ser = serial.Serial(port='COMX', baudrate=115200, timeout=1)

def sendChar():
    charToSend = input("Enter char: ")
    ser.write(str(charToSend).encode())
    sentChar = ser.readline().decode()
    return sentChar


for n in range(1):
    print(sendChar)
    
ser.close()
    



