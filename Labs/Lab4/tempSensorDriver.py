'''@file                    tempSensorDriver.py
   @brief                   Brief doc for  tempSensorDriver.py
   @details                 Detailed doc for  tempSensorDriver.py
   @author                  Anthony Vuong
   @date                    May 15, 2021
'''
import pyb, utime, sys
from mcp9808 import mcp


#Function to convert seconds to hours/minutes/seconds/milliseconds
def timeStampConversion(totalSeconds):
    '''@brief           Converts seconds elapsed to time stamp
       @details         Detailed info on converting seconds to formatted time stamp
       @return          Current time of test converted into hours/minutes/seconds/milliseconds
    '''
    hour = 0
    minute = 0
    seconds = 0
    
    hour = int(totalSeconds / 3600)
    totalSeconds = float(totalSeconds - (hour * 3600))
    minute = int(totalSeconds / 60)
    totalSeconds = float(totalSeconds - (minute * 60))
   
    seconds = int(totalSeconds)
    
    mSeconds = float(totalSeconds - seconds) * 1000

    return str(hour) + ":" + str(minute) + ":" + str(seconds) + ":" + str(int(mSeconds))
    

#MCP9808 Driver
if __name__ == "__main__":
    
    #Initialize start of 8 hour data gathering
    minuteCounter = 0
    #Initialize I2C bus on pins PC0 PC1
    bus = 3
    #Initalize device address; found using i2c.scan() - 24 decimal, 0x18 hex
    devAddress = 24
    #Initialize data gathering start time
    testStarttime = utime.ticks_ms()
    
    
    #Configure I2C pins
    pinSDA = pyb.Pin(pyb.Pin.board.PC1, pyb.Pin.PULL_UP)   #pullup
    pinSCL = pyb.Pin(pyb.Pin.board.PC0)
        
    
    #Initialize I2C object on bus 3, and master
    i2cObj = pyb.I2C(bus)
    i2cObj.init(pyb.I2C.MASTER, baudrate=115200)
    
    
    #Initialize MCP9808 object with I2C object above
    mcpDevice = mcp(devAddress, i2cObj)

    #Variable to call internal temp sensor function
    adcall = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
    
    #Run program until user presses ctrl-c or 8 hours have elapsed
    while True: 
        #Surround with try/catch to wait for exit command
        try:
            
            # 1) Wait for seconds
            # 2) Gather ambient temp, STM32 internal temp, and current time
            # 3) Write all three data to csv file called tempData.csv
            utime.sleep(60)
            curr_temp_fahren = mcpDevice.fahrenheit()
            internal_temp = adcall.read_core_temp()
            curr_time = utime.ticks_ms()
            timeSeconds = utime.ticks_diff(curr_time, testStarttime)  / 1000
            timeStamp = timeStampConversion(timeSeconds)
            with open('tempData.csv',"a") as file:
                file.write('{:},{:},{:}\n'.format(timeStamp, internal_temp, curr_temp_fahren))
            
            
            #Increment minuteCounter until 480 == 8 hours
            minuteCounter += 1
            
            #Exit program gracefully after 8 hours have passed
            if(minuteCounter == 480):
                print("8 hours have elapsed. All Done")
                sys.exit()
                
        #Exit gracefully if user presses ctrl-c
        except KeyboardInterrupt:
            print("All done!")
            sys.exit()