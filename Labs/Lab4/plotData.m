%[x, y, z] = xlsread('tempData.csv');

% data = readtable('tempData.csv');
% 
% x = data{:, 1};
% y = data{:, 2};
% z = data{:, 3};
% 



time = [480];

for i = 1 : 480
    time(i) = i;
end

figure
plot(time, z, time, y);
title('Time vs Ambient/Internal Temperature');
ylabel('Ambient(Blue) / Internal(Orange)')
xlabel('Time(minute)')
