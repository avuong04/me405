'''@file                    mcp9808.py
   @brief                   Brief doc for  mcp9808.py
   @details                 Detailed doc for  mcp9808.py
   @author                  Anthony Vuong
   @date                    May 15, 2021
'''

from micropython import const

#REG POINTERS
TEMP_REG = const(5)
MANUFACTURER_ID_REG = const(6)

#Manufacturer ID found on register pointer
MANUFACTURER_ID = b'\x00T'


#Class for temperature sensor MCP9808
class mcp:
    
    def __init__(self, devAddress, i2cObj):
        '''@brief           Constructor for MCP9808 driver
           @details         Detailed info on MCP9808 driver constructor
        '''
        
        ## @brief           The I2C object 
        #  @details         I2C object passed to module
        #
        self.i2cObj = i2cObj
        
        ## @brief           The address of device connected to NUCLEO 
        #  @details         I2C address of device
        #
        self.address = devAddress
        
        #check the device manufacturer id before initializing
        self.check()
        
    def check(self):
        '''@brief           Checks for device manufacturer ID
           @details         Detailed info on the manufacturer's ID in bytes
           @return          Throws exception if manufacturer ID is not correct
        '''
        data = self.readfromDev(2, MANUFACTURER_ID_REG)
        if data != MANUFACTURER_ID:
            #raise Exception("Manufacturer ID is invalid!")
            print("Manufacturer ID is invalid!")
    
    
    def fahrenheit(self):
       '''@brief           Gets the ambient temperature converted to fahrenheit
           @details         Detailed info on ambient temperature 
           @return          Temperature fahrenheit
       '''
       tempCel = self.celcius()
       #Equation for fahrenheit degress
       tempFahr = float(tempCel * (9 / 5) + 32)
       return tempFahr
        
     
    def celcius(self):
        '''@brief           Gets the ambient temperature converted to celcius
           @details         Detailed info on ambient temperature 
           @return          Temperature celcius
        '''
        #2 bytees read and stored in data
        data = self.readfromDev(2, TEMP_REG)
        
        #First byte contains bits we do not need and should not alter
        upper = (data[0] & 0x0f) << 4
        lower = data[1] / 16
        
        if upper & 0x80 == 0x80:
            temp = 256 - (upper + lower)
        else:
            temp = upper + lower
        
        return temp
    
    def writetoDev(self, data, memReg):
        '''@brief           Writes number of bytes to device register
           @details         Detailed info on device register data
           @return          None
        '''
        self.i2cObj.send(data, self.address, memReg)
        
    def readfromDev(self, data, memReg):
        '''@brief           Reads data from device register
           @details         Detailed info on register data
           @return          Data on specified data register
        '''
        data = self.i2cObj.mem_read(2, self.address, memReg)
        return data
        
   

    
    
    
    
 
